package com.FrontEndService.controller;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class LandingController {

    @GetMapping("")
    public String index() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            return "redirect:/landing";
        }
        return "index";
    }

    @GetMapping("/landing")
    public String landing(Model model) {
        String dateToday = dateFormatter(getDateToday());
        model.addAttribute("today",dateToday);
        model.addAttribute("username", getUsername());
        model.addAttribute("title" , "Landing");
        return "landing";
    }

    @GetMapping("/rekap")
    public String rekapBiasa(Model model) {
        String username = "bobby";
        String dateToday = dateFormatter(getDateToday());
        model.addAttribute("today",dateToday);
        model.addAttribute("username", getUsername());
        model.addAttribute("title" , "Rekap");
        return "rekap";
    }

    @GetMapping("/kategori")
    public String kategori(Model model){
        String dateToday = dateFormatter(getDateToday());
        model.addAttribute("today",dateToday);
        model.addAttribute("username", getUsername());
        model.addAttribute("title" , "Kategori");
        return "kategori";
    }

    public LocalDate getDateToday() {
        return LocalDate.now();
    }

    public String dateFormatter(LocalDate date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMMM yyyy");
        String formattedDate = date.format(formatter);
        return formattedDate;
    }

    @GetMapping("/wishlist")
    public String wishlist(Model model) {
        String dateToday = dateFormatter(getDateToday());
        model.addAttribute("today",dateToday);
        model.addAttribute("username", getUsername());
        model.addAttribute("title" , "Wishlist");
        return "wishlist";
    }

    public String getUsername() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            String currentUserName = authentication.getName();
            return currentUserName;
        }
        return "No User";
    }


}

