var root = "https://w-g-wishlist.herokuapp.com";
var user = $("#userToken").val();

$(function (e) {
    $("#formWish").on('submit', function(event) {
        var form = $('#formWish')[0];
        var data = new FormData(form);
        data.append("user", user);
        console.log(data);
        event.preventDefault();
        $.ajax({
            type : "POST",
            url : root + "/add-wish/",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            data : data,
            success: function(response) {
                console.log(response);
                getWL();
                $("#title").val("");
                $("#price").val("");
                alert("wishlist berhasil ditambahkan!");
            },
            error: function (e) {
                console.log(e.responseText);
                console.log("ERROR : ", e);
            }
        });
    });
    console.log(user);

    var getWL = function(e) {
        $.ajax({
            url : root + "/wishlist/" + user,
            dataType:"JSON",
            success : function (response) {
                var tabel = "";
                var i = 0;
                while (response[i] != undefined) {
                        tabel += "<tr><td scope='row'>" + (i+1) + ".</td>" +
                            "<td scope='row'>" + response[i].title + "</td>" +
                            "<td scope='row'>" + response[i].category + "</td>" +
                            "<td scope='row'>" + response[i].price + "</td>" +
                            "</td></tr>";
                        i += 1;
                }
                $("#dataWL").html(tabel);
            }
        });
    };

    getWL(e);
});