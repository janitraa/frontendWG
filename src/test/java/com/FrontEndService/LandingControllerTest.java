package com.FrontEndService;

import com.FrontEndService.controller.LandingController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = LandingController.class, properties = "spring.profiles.active=test")
public class LandingControllerTest {

    @Autowired
    private MockMvc landingController;

    @Test
    public void landingPage() throws Exception {
        System.out.println("Bobby");
        landingController.perform(get("/landing"))
                .andExpect(status().isOk());
    }
}
