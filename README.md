# Wallet Guardian
Kelompok: `C-6`

Gitlab | Heroku
-
- Frontend: [gitlab](https://gitlab.com/janitraa/frontendWG) | [heroku](http://wallet-guardian.herokuapp.com)
- Backend (transaksi): [gitlab](https://gitlab.com/janitraa/wallet-guardian) | [heroku](http://w-g-backend.herokuapp.com)
- Backend (wishlist): [gitlab](https://gitlab.com/rd0/walletguardian-backend-wishlist) | [heroku](http://w-g-wishlist.herokuapp.com)

## References
- https://spring.io/guides/tutorials/spring-boot-oauth2/#_social_login_logout


![pipeline status](https://gitlab.com/janitraa/frontendWG/badges/master/pipeline.svg)

![coverage report](https://gitlab.com/janitraa/frontendWG/badges/master/coverage.svg)
